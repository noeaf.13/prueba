package com.noe.test.ui.informacionSerie

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.noe.test.models.ResponseRequest
import com.noe.test.providers.SeriesInteractor

class InformacionSerieViewModel : ViewModel() {
    val responseInforamacionSerie = MutableLiveData<ResponseRequest>()
    val responseVideosSerie = MutableLiveData<ResponseRequest>()

    private val interactor: SeriesInteractor = SeriesInteractor()

    fun getInformacionSerie(idSerie:Int) {
        interactor.getInformacion( idSerie ) {
            this.responseInforamacionSerie.postValue( it )
        }
    }
    fun getVideosSerie( idSerie:Int ){
        interactor.getVideos( idSerie ) {
            this.responseVideosSerie.postValue( it )
        }
    }
}