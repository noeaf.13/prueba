package com.noe.test.common.data

import android.content.Context
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley

class MovieApi constructor( context: Context ) {

    companion object{
        @Volatile
        private var INSTANCE: MovieApi? = null
        fun getInstance(context: Context) = INSTANCE ?: synchronized(this){
            INSTANCE ?: MovieApi(context).also {
                INSTANCE = it
            }
        }
    }

    private val requestQueue : RequestQueue by lazy{
        Volley.newRequestQueue(context.applicationContext)
    }
    fun <T> addToRequestQueue( req: Request<T> ){
        /*req.retryPolicy = DefaultRetryPolicy(
            Constants.DEFAULT_TIMEOUT,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )*/
        requestQueue.add(req)
    }
}