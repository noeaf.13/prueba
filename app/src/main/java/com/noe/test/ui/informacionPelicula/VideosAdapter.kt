package com.noe.test.ui.informacionPelicula

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.noe.test.R
import com.noe.test.databinding.ItemVideoBinding
import com.noe.test.models.VideoEntity
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener


class VideosAdapter(private var listaVideos: MutableList<VideoEntity>) : RecyclerView.Adapter<VideosAdapter.ViewHolder>() {
    private lateinit var mContext: Context
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val bindingItem = ItemVideoBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_video, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: kotlin.Int) {
        val video = listaVideos.get(position);
        with(holder){
            bindingItem.titulovideo.text = video.name
            bindingItem.video.addYouTubePlayerListener(object : AbstractYouTubePlayerListener() {
                override fun onReady(youTubePlayer: com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer) {
                    val videoId = video.key
                    youTubePlayer.loadVideo(videoId, 0f)
                    if( listaVideos.indexOf( video ) > 0 ) youTubePlayer.pause() else youTubePlayer.play()
                }
            })
        }
    }
    override fun getItemCount(): Int {
        return this.listaVideos.size
    }

    fun setVideos( videos: MutableList<VideoEntity> ) {
        this.listaVideos = videos;
        notifyDataSetChanged()
    }

}