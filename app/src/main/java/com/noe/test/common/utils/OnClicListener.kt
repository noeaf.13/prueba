package com.noe.test.common.utils

import com.noe.test.models.PeliculaEntity

interface OnClicListener {
    fun onClicInfo(pelicula: PeliculaEntity)
}