package com.noe.test.ui.informacionSerie

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.chip.Chip
import com.noe.test.R
import com.noe.test.common.utils.Constantes
import com.noe.test.databinding.InformacionSerieFragmentBinding
import com.noe.test.models.InformacionSerieEntity
import com.noe.test.models.PeliculaEntity
import com.noe.test.models.Season
import com.noe.test.models.VideoEntity
import com.noe.test.ui.informacionPelicula.VideosAdapter

class InformacionSerieFragment : Fragment() {

    private lateinit var mBinding: InformacionSerieFragmentBinding
    private val args : InformacionSerieFragmentArgs by navArgs()
    private lateinit var serie : PeliculaEntity

    private val informacionViewModel: InformacionSerieViewModel by viewModels()
    private lateinit var serieInfo: InformacionSerieEntity;

    private var videosList: MutableList<VideoEntity> = mutableListOf()
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var videosAdapter: VideosAdapter

    private var temporadaList: MutableList<Season> = mutableListOf()
    private lateinit var mLayoutManagerTemporada: RecyclerView.LayoutManager
    private lateinit var temporadaAdapter: TemporadaAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = InformacionSerieFragmentBinding.inflate( inflater, container, false )
        serie = args.serie
        return  mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cargaInformacion()
        setupViewModel()
        setupUI()
        initRecycler()
    }

    private fun cargaInformacion() {
        informacionViewModel.getInformacionSerie( serie.id );
    }

    private fun setupViewModel() {
        informacionViewModel.responseInforamacionSerie.observe(viewLifecycleOwner) {
            if (it.response) {
                this.serieInfo = it.data as InformacionSerieEntity
                cargarGeneros()
                cargarTemporadas()
            }
        }
        informacionViewModel.responseVideosSerie.observe(viewLifecycleOwner){
            if (it.response){
                this.videosList.addAll( it.data as MutableList<VideoEntity> )
                if( this.videosList.size > 0 ) videosAdapter.setVideos(this.videosList)
                loaderVideos()
            }
        }
    }

    private fun loaderVideos() {
        if ( this.videosList.size == 0 ) {
            mBinding.loaderTxt.text = "Sin videos disponibles"
            mBinding.loader.visibility = View.GONE
        }else{
            mBinding.loaderVideos.visibility = View.GONE
            mBinding.videos.visibility = View.VISIBLE
        }
    }

    private fun cargarGeneros() {
        this.serieInfo.genres.forEach {
            val chip = Chip(context)
            chip.text = it.name
            chip.isClickable = false
            mBinding.chipsgGeneros.addView(chip)
        }
    }

    private fun cargarTemporadas() {
        this.temporadaList.addAll( this.serieInfo.seasons )
        temporadaAdapter.setTemporadas( this.temporadaList )
    }

    private fun setupUI() {
        (activity as AppCompatActivity).supportActionBar?.hide()
        with(mBinding) {
            back.setOnClickListener {
                requireActivity().onBackPressed()
            }
            titulo.text = serie.name
            Glide.with(requireContext())
                .load( "${Constantes.URL_IMAGENES}/w500${serie.poster_path}" )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .error(R.drawable.pelicula_sin_imagen)
                .into( imgPoster )
            Glide.with(requireContext())
                .load( "${Constantes.URL_IMAGENES}/original${serie.backdrop_path}" )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .placeholder(R.drawable.sin_senial)
                .error(R.drawable.sin_senial)
                .into( imgLandscape )
            tvResumenInfo.text = serie.overview
            tvRatingInfo.text = serie.vote_average.toString()
            tvVotosInfo.text = serie.vote_count.toString()
        }
    }

    private fun initRecycler() {
        mLayoutManagerTemporada = LinearLayoutManager( context, LinearLayoutManager.HORIZONTAL, true )
        temporadaAdapter = TemporadaAdapter( temporadaList )
        mBinding.temporadas.apply {
            setHasFixedSize( true )
            adapter = temporadaAdapter
            layoutManager = mLayoutManagerTemporada
        }


        mLayoutManager = LinearLayoutManager( context )
        videosAdapter = VideosAdapter( videosList )
        informacionViewModel.getVideosSerie(serie.id);
        mBinding.videos.apply {
            setHasFixedSize( true )
            adapter = videosAdapter
            layoutManager = mLayoutManager
        }
    }

}