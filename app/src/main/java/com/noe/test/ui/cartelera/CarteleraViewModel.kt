package com.noe.test.ui.cartelera

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.noe.test.models.ResponseRequest
import com.noe.test.providers.PeliculasInteractor
import com.noe.test.providers.SeriesInteractor

class CarteleraViewModel : ViewModel() {
    val responseCarteleraPeliculas = MutableLiveData<ResponseRequest>()
    val responseSeriesAlAire = MutableLiveData<ResponseRequest>()
    private val interactorPeliculas: PeliculasInteractor = PeliculasInteractor()
    private val interactorSeries: SeriesInteractor = SeriesInteractor()

    fun getListaCatelera() {
        interactorPeliculas.getListaCatelera {
            this.responseCarteleraPeliculas.postValue( it )
        }
    }

    fun getListaSeries() {
        interactorSeries.getListaSeries {
            this.responseSeriesAlAire.postValue( it )
        }
    }

}