package com.noe.test.models

data class Genre(
    val id: Int,
    val name: String
)