package com.noe.test.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.noe.test.R
import com.noe.test.common.utils.Constantes
import com.noe.test.common.utils.OnClicListener
import com.noe.test.databinding.ItemPeliculaBinding
import com.noe.test.models.PeliculaEntity

class PeliculasAdapter(private var listaPeliculas: MutableList<PeliculaEntity>, private var listener:OnClicListener) : RecyclerView.Adapter<PeliculasAdapter.ViewHolder>() {
    private lateinit var mContext: Context
    inner class ViewHolder(view:View) : RecyclerView.ViewHolder(view) {
        val bindingItem = ItemPeliculaBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_pelicula, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pelicula = listaPeliculas.get(position);
        with(holder){
            bindingItem.cardTitulo.text = pelicula.title ?: pelicula.name;
            bindingItem.cardRank.text = pelicula.vote_average.toString()
            Glide.with(mContext)
                .load( "${Constantes.URL_IMAGENES}/w500${pelicula.poster_path}" )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .error(R.drawable.pelicula_sin_imagen)
                .into( bindingItem.imageView )

            bindingItem.root.setOnClickListener {
                listener.onClicInfo( pelicula )
            }
        }
    }

    override fun getItemCount(): Int {
        return this.listaPeliculas.size
    }

    fun setPeliculas( peliculas: MutableList<PeliculaEntity> ) {
        this.listaPeliculas = peliculas;
        notifyDataSetChanged()
    }
}