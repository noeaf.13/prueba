package com.noe.test.providers

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.noe.test.common.utils.Constantes
import com.noe.test.models.InformacionPeliculaEntity
import com.noe.test.models.PeliculaEntity
import com.noe.test.models.ResponseRequest
import com.noe.test.models.VideoEntity

class PeliculasInteractor {
    fun getListaCatelera( callback: ( ResponseRequest ) -> Unit ) {
        var peliculasList: MutableList<PeliculaEntity>? = null
        GeneralRequest.fnRequest( "GET", Constantes.PATH_PELICULAS_CARTELERA, "?api_key=${Constantes.API_KEY}&language=es&include_image_language=es", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<MutableList<PeliculaEntity>>(){}.type
                val listPeliculas = resp.optJSONArray("results")?.toString()
                peliculasList = Gson().fromJson( listPeliculas, mutableListType )
            }
            callback( ResponseRequest(true, peliculasList) )
        }, {
            it.printStackTrace()
        }  )
    }


    fun getListaPopular( callback: ( ResponseRequest ) -> Unit ) {
        var peliculasList: MutableList<PeliculaEntity>? = null
        GeneralRequest.fnRequest( "GET", Constantes.PATH_PELICULAS_POPULARES, "?api_key=${Constantes.API_KEY}&language=es&region=mx", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<MutableList<PeliculaEntity>>(){}.type
                val listPeliculas = resp.optJSONArray("results")?.toString()
                peliculasList = Gson().fromJson( listPeliculas, mutableListType )
            }
            callback( ResponseRequest(true, peliculasList) )
        }, {
            it.printStackTrace()
        }  )
    }

    fun getInformacion(idPelicula: Int, callback: ( ResponseRequest ) -> Unit) {
        var pelicula: InformacionPeliculaEntity? = null
        val url = Constantes.PATH_PELICULA_INFORMACION.replace(":id:", idPelicula.toString())
        GeneralRequest.fnRequest( "GET", url, "?api_key=${Constantes.API_KEY}&language=es", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<InformacionPeliculaEntity>(){}.type
                pelicula = Gson().fromJson( resp.toString(), mutableListType )
            }
            callback( ResponseRequest(true, pelicula) )
        }, {
            it.printStackTrace()
        }  )
    }

    fun getVideos(idPelicula: Int, callback: ( ResponseRequest ) -> Unit) {
        var videosList: MutableList<VideoEntity>? = null
        val url = Constantes.PATH_PELICULA_VIDEOS.replace(":id:", idPelicula.toString())
        GeneralRequest.fnRequest( "GET", url, "?api_key=${Constantes.API_KEY}&language=es-MX", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<MutableList<VideoEntity>>(){}.type
                val listaVideos = resp.optJSONArray("results")?.toString()
                videosList = Gson().fromJson( listaVideos, mutableListType )
            }
            callback( ResponseRequest(true, videosList) )
        }, {
            it.printStackTrace()
        }  )
    }

}