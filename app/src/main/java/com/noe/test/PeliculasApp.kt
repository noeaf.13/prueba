package com.noe.test

import android.app.Application
import com.noe.test.common.data.MovieApi

class PeliculasApp : Application() {
    companion object{
        lateinit var movieApi: MovieApi
        lateinit var instance: Application
    }

    override fun onCreate() {
        super.onCreate()
        movieApi = MovieApi(this)
        instance = this
    }
}