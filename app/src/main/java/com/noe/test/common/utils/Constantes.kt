package com.noe.test.common.utils

object Constantes {
    const val URL_MovieDB = "https://api.themoviedb.org/3"
    const val URL_IMAGENES = "https://image.tmdb.org/t/p"
    const val API_KEY = "24f6fa605c47acf40b1118161d6067a2"

    const val KEY_YOUTUBE = "AIzaSyCsZQu6vQbre6kGNlcUL9J9mTJWbo9SKVc"

    const val PATH_PELICULAS_CARTELERA = "/movie/now_playing"
    const val PATH_PELICULAS_POPULARES = "/movie/popular"
    const val PATH_PELICULA_INFORMACION = "/movie/:id:"
    const val PATH_PELICULA_VIDEOS = "/movie/:id:/videos"

    const val PATH_SERIES_CARTELERA = "/tv/on_the_air"
    const val PATH_SERIES_POPULARES = "/tv/popular"
    const val PATH_SERIE_INFORMACION = "/tv/:id:"
    const val PATH_SERIE_VIDEOS = "/tv/:id:/videos"


}