package com.noe.test.providers

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.noe.test.common.utils.Constantes
import com.noe.test.models.*

class SeriesInteractor {
    fun getListaSeries( callback: (ResponseRequest) -> Unit ) {
        var seriesList: MutableList<PeliculaEntity>? = null
        GeneralRequest.fnRequest( "GET", Constantes.PATH_SERIES_CARTELERA, "?api_key=${Constantes.API_KEY}&language=es&include_image_language=es", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<MutableList<PeliculaEntity>>(){}.type
                val listSeries = resp.optJSONArray("results")?.toString()
                seriesList = Gson().fromJson( listSeries, mutableListType )
            }
            callback( ResponseRequest(true, seriesList) )
        }, {
            it.printStackTrace()
        }  )
    }
    fun getListaPopular( callback: ( ResponseRequest ) -> Unit ) {
        var seriesList: MutableList<PeliculaEntity>? = null
        GeneralRequest.fnRequest( "GET", Constantes.PATH_SERIES_POPULARES, "?api_key=${Constantes.API_KEY}&language=es&region=mx", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<MutableList<PeliculaEntity>>(){}.type
                val listSeries = resp.optJSONArray("results")?.toString()
                seriesList = Gson().fromJson( listSeries, mutableListType )
            }
            callback( ResponseRequest(true, seriesList) )
        }, {
            it.printStackTrace()
        }  )
    }

    fun getInformacion(idSerie: Int, callback: ( ResponseRequest ) -> Unit) {
        var serie: InformacionSerieEntity? = null
        val url = Constantes.PATH_SERIE_INFORMACION.replace(":id:", idSerie.toString())
        GeneralRequest.fnRequest( "GET", url, "?api_key=${Constantes.API_KEY}&language=es", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<InformacionSerieEntity>(){}.type
                serie = Gson().fromJson( resp.toString(), mutableListType )
            }
            callback( ResponseRequest(true, serie) )
        }, {
            it.printStackTrace()
        }  )
    }

    fun getVideos(idSerie: Int, callback: ( ResponseRequest ) -> Unit) {
        var videosList: MutableList<VideoEntity>? = null
        val url = Constantes.PATH_SERIE_VIDEOS.replace(":id:", idSerie.toString())
        GeneralRequest.fnRequest( "GET", url, "?api_key=${Constantes.API_KEY}&language=es-MX", { resp ->
            val success = resp.optBoolean("success", true)
            if (success) {
                val mutableListType = object : TypeToken<MutableList<VideoEntity>>(){}.type
                val listaVideos = resp.optJSONArray("results")?.toString()
                videosList = Gson().fromJson( listaVideos, mutableListType )
            }
            callback( ResponseRequest(true, videosList) )
        }, {
            it.printStackTrace()
        }  )
    }
}