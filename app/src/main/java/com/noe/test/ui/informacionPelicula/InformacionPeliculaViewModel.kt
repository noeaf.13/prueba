package com.noe.test.ui.informacionPelicula

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.noe.test.models.ResponseRequest
import com.noe.test.providers.PeliculasInteractor

class InformacionPeliculaViewModel : ViewModel() {
    val responseInforamacionPelicula = MutableLiveData<ResponseRequest>()
    val responseVideosPelicula = MutableLiveData<ResponseRequest>()

    private val interactor: PeliculasInteractor = PeliculasInteractor()

    fun getInformacionPelicula(idPelicula:Int) {
        interactor.getInformacion( idPelicula ) {
            this.responseInforamacionPelicula.postValue( it )
        }
    }
    fun getVideosPelicula( idPelicula:Int ){
        interactor.getVideos( idPelicula ) {
            this.responseVideosPelicula.postValue( it )
        }
    }
}