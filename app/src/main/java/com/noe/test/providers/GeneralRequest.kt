package com.noe.test.providers

import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.Response.Listener
import com.android.volley.toolbox.JsonObjectRequest
import com.noe.test.PeliculasApp
import com.noe.test.common.utils.Constantes
import org.json.JSONObject

object GeneralRequest {
    fun fnRequest( method: String, path: String, params: Any?, listener: Listener<JSONObject>, errorListener: Response.ErrorListener ) {
        try {
            var paramsJSON: JSONObject? = null
            var paramsQuery = ""

            if ( params != null && method == "GET" ) {
                paramsQuery = params as String
            }else if ( params != null && method == "POST" ) {
                paramsJSON = params as JSONObject
            }
            val url = "${Constantes.URL_MovieDB}${path}${paramsQuery}"
            val _method = if ( method == "POST" ) Request.Method.POST else Request.Method.GET
            val jsonObject = JsonObjectRequest( _method, url, paramsJSON, listener, errorListener )
            PeliculasApp.movieApi.addToRequestQueue( jsonObject )
        } catch (e:Exception){
            e.printStackTrace()
        }
    }

}