package com.noe.test.models

data class ResponseRequest(
    var response: Boolean,
    val data: Any?,
    var errorService: Boolean? = false
) {
    constructor() : this (
        response = false,
        data = null,
        errorService = false
    )
}