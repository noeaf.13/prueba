package com.noe.test.ui.populares

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.noe.test.models.ResponseRequest
import com.noe.test.providers.PeliculasInteractor
import com.noe.test.providers.SeriesInteractor

class PopularViewModel : ViewModel() {
    val responsePopular = MutableLiveData<ResponseRequest>()
    val responseSeries = MutableLiveData<ResponseRequest>()
    private val interactor: PeliculasInteractor = PeliculasInteractor()
    private val interactorSeries: SeriesInteractor = SeriesInteractor()
    fun getListaPopular(){
        interactor.getListaPopular {
            this.responsePopular.postValue( it )
        }
    }
    fun getListaSeries() {
        interactorSeries.getListaPopular {
            this.responseSeries.postValue( it )
        }
    }
}