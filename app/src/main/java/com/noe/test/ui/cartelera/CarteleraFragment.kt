package com.noe.test.ui.cartelera

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.noe.test.common.utils.OnClicListener
import com.noe.test.models.PeliculaEntity
import com.noe.test.ui.PeliculasAdapter
import com.noe.test.databinding.CarteleraFragmentBinding

class CarteleraFragment : Fragment(), OnClicListener {
    private var navController : NavController? = null;
    private lateinit var mBinding: CarteleraFragmentBinding
    private val carteleraViewModel: CarteleraViewModel by viewModels()

    private var listaPeliculas: MutableList<PeliculaEntity> = mutableListOf()
    private lateinit var mLayoutManagerPeliculas: RecyclerView.LayoutManager
    private lateinit var peliculasAdapter : PeliculasAdapter

    private var listaSeries: MutableList<PeliculaEntity> = mutableListOf()
    private lateinit var mLayoutManagerSeries: RecyclerView.LayoutManager
    private lateinit var seriesAdapter : PeliculasAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = CarteleraFragmentBinding.inflate( inflater, container, false )
        return  mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        navController = Navigation.findNavController(view)

        loadListaCartelera();
        setupViewModel()
        initRecycler()
    }

    private fun loadListaCartelera() {
        carteleraViewModel.getListaCatelera();
        carteleraViewModel.getListaSeries();
    }

    private fun setupViewModel() {
        carteleraViewModel.responseCarteleraPeliculas.observe( viewLifecycleOwner ) { resp ->
            if ( resp.response ) {
                this.listaPeliculas = resp.data as MutableList<PeliculaEntity>
                peliculasAdapter.setPeliculas(this.listaPeliculas);
            }
        }
        carteleraViewModel.responseSeriesAlAire.observe( viewLifecycleOwner ) { resp ->
            if ( resp.response ) {
                this.listaSeries = resp.data as MutableList<PeliculaEntity>
                seriesAdapter.setPeliculas(this.listaSeries);
            }
        }
    }

    private fun initRecycler() {
        peliculasAdapter = PeliculasAdapter( listaPeliculas, this )
        mLayoutManagerPeliculas = GridLayoutManager( context, 10 )
        loadListaCartelera()
        mBinding.carteleraRv.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManagerPeliculas
            adapter = peliculasAdapter
        }

        seriesAdapter = PeliculasAdapter( listaSeries, this )
        mLayoutManagerSeries = GridLayoutManager( context, 10 )
        mBinding.carteleraRvSeries.apply {
            setHasFixedSize(true)
            layoutManager = mLayoutManagerSeries
            adapter = seriesAdapter
        }
    }

    override fun onClicInfo(pelicula: PeliculaEntity) {
        if( pelicula.title != null ) {
            val directions = CarteleraFragmentDirections.actionNavigationCarteleraToInformacionFragment( pelicula )
            navController!!.navigate( directions )
        }else{
            val directions = CarteleraFragmentDirections.actionCarteleraFragmentToInformacionSerieFragment( pelicula )
            navController!!.navigate( directions )
        }
    }
}
