package com.noe.test.ui.informacionPelicula

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.google.android.material.chip.Chip
import com.noe.test.R
import com.noe.test.common.utils.Constantes
import com.noe.test.databinding.InformacionFragmentBinding
import com.noe.test.models.InformacionPeliculaEntity
import com.noe.test.models.PeliculaEntity
import com.noe.test.models.VideoEntity

class InformacionPeliculaFragment : Fragment() {
    private lateinit var mBinding: InformacionFragmentBinding
    private val args : InformacionPeliculaFragmentArgs by navArgs()
    private lateinit var pelicula : PeliculaEntity

    private val informacionViewModel: InformacionPeliculaViewModel by viewModels()
    private lateinit var peliculaInfo: InformacionPeliculaEntity;

    private var videosList: MutableList<VideoEntity> = mutableListOf()
    private lateinit var mLayoutManager: RecyclerView.LayoutManager
    private lateinit var videosAdapter: VideosAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = InformacionFragmentBinding.inflate( inflater, container, false )
        pelicula = args.pelicula
        return  mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        cargaInformacion()
        setupViewModel()
        setupUI()
        initRecycler()
    }

    private fun cargaInformacion() {
        informacionViewModel.getInformacionPelicula( pelicula.id );
    }

    private fun setupViewModel() {
        informacionViewModel.responseInforamacionPelicula.observe(viewLifecycleOwner) {
            if (it.response) {
                this.peliculaInfo = it.data as InformacionPeliculaEntity
                cargarGeneros()
            }
        }
        informacionViewModel.responseVideosPelicula.observe(viewLifecycleOwner){
            if (it.response){
                this.videosList.addAll( it.data as MutableList<VideoEntity> )
                if( this.videosList.size > 0 ) videosAdapter.setVideos(this.videosList)
                loaderVideos()
            }
        }
    }

    private fun loaderVideos() {
        if ( this.videosList.size == 0 ) {
            mBinding.loaderTxt.text = "Sin videos disponibles"
            mBinding.loader.visibility = View.GONE
        }else{
            mBinding.loaderVideos.visibility = View.GONE
            mBinding.videos.visibility = View.VISIBLE
        }
    }

    private fun cargarGeneros() {
        this.peliculaInfo.genres.forEach {
            val chip = Chip(context)
            chip.text = it.name
            chip.isClickable = false
            mBinding.chipsgGeneros.addView(chip)
        }
    }

    private fun setupUI() {
        (activity as AppCompatActivity).supportActionBar?.hide()
        with(mBinding) {
            back.setOnClickListener {
                requireActivity().onBackPressed()
            }
            titulo.text = pelicula.title
            Glide.with(requireContext())
                .load( "${Constantes.URL_IMAGENES}/w500${pelicula.poster_path}" )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .error(R.drawable.pelicula_sin_imagen)
                .into( imgPoster )
            Glide.with(requireContext())
                .load( "${Constantes.URL_IMAGENES}/original${pelicula.backdrop_path}" )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .placeholder(R.drawable.sin_senial)
                .error(R.drawable.sin_senial)
                .into( imgLandscape )
            tvResumenInfo.text = pelicula.overview
            tvRatingInfo.text = pelicula.vote_average.toString()
            tvVotosInfo.text = pelicula.vote_count.toString()
        }
    }

    private fun initRecycler() {
        mLayoutManager = LinearLayoutManager( context )
        videosAdapter = VideosAdapter( videosList )
        informacionViewModel.getVideosPelicula(pelicula.id);
        mBinding.videos.apply {
            setHasFixedSize( true )
            adapter = videosAdapter
            layoutManager = mLayoutManager
        }
    }
}