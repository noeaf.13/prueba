package com.noe.test.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import kotlin.Int
@Parcelize
data class PeliculaEntity(
    val adult: Boolean,
    val backdrop_path: String,
    val genre_ids: List<Int>,
    val id: Int,
    val original_language: String,
    val original_title: String,
    val overview: String,
    val popularity: Double,
    val poster_path: String,
    val release_date: String,
    val title: String?,
    val name: String?,
    val video: Boolean,
    val vote_average: Double,
    val vote_count: Int
) : Parcelable {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PeliculaEntity

        if (id != other.id) return false
        if (title != other.title) return false
        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        var result = id
        result = 31 * result + (title?.hashCode() ?: 0)
        result = 31 * result + (name?.hashCode() ?: 0)
        return result
    }
}