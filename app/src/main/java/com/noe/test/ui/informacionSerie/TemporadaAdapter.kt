package com.noe.test.ui.informacionSerie

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.noe.test.R
import com.noe.test.common.utils.Constantes
import com.noe.test.databinding.ItemTemporadaBinding
import com.noe.test.models.Season

class TemporadaAdapter(private var listaTemporadas: MutableList<Season>) : RecyclerView.Adapter<TemporadaAdapter.ViewHolder>() {
    private lateinit var mContext: Context
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val bindingItem = ItemTemporadaBinding.bind(view)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        mContext = parent.context
        val view = LayoutInflater.from(mContext).inflate(R.layout.item_temporada, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: kotlin.Int) {
        val temporada = listaTemporadas.get(position);
        println( temporada )
        with(holder){
            bindingItem.tituloInfo.text = temporada.name
            bindingItem.episodiosInfo.text = temporada.episode_count.toString()
            bindingItem.temporadaInfo.text = temporada.season_number.toString()
            bindingItem.resumenTemporadaInfo.text = temporada.overview
            Glide.with(mContext)
                .load( "${Constantes.URL_IMAGENES}/w500${temporada.poster_path}" )
                .diskCacheStrategy( DiskCacheStrategy.ALL )
                .centerCrop()
                .placeholder(R.drawable.sin_senial)
                .error(R.drawable.sin_senial)
                .into( bindingItem.posterTemporada )
        }
    }
    override fun getItemCount(): Int {
        return this.listaTemporadas.size
    }

    fun setTemporadas( temporadas: MutableList<Season> ) {
        println( temporadas )
        this.listaTemporadas = temporadas;
        notifyDataSetChanged()
    }
}